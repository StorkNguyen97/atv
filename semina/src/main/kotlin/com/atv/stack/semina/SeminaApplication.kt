package com.atv.stack.semina

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SeminaApplication

fun main(args: Array<String>) {
    runApplication<SeminaApplication>(*args)
}
