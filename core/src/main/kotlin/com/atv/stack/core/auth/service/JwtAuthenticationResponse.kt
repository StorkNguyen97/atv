package com.atv.stack.core.auth.service

import com.atv.stack.core.auth.JwtUser


class JwtAuthenticationResponse {
     var token: String = ""
    //private var id: Int = 0
     var username: String? =  ""
     var email: String? = "";

    constructor ( token: String,  userDetails: JwtUser){
        this.token = token;
        this.username = userDetails.getUsername()
        this.email = userDetails.getEmail()
    }

    constructor(refreshedToken: String){
        this.token = refreshedToken
    }
}

