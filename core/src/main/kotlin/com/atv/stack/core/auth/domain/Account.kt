package com.atv.stack.core.auth.domain


import javax.persistence.*

@Entity(name = "account")
class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Int = 0
    var name: String? = ""
    var email: String? = ""
    @Lob
    var password: String? = ""

    constructor()

}
