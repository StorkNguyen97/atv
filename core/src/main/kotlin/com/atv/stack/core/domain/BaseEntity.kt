package com.atv.stack.core.domain

import org.hibernate.annotations.UpdateTimestamp
import org.springframework.data.annotation.CreatedDate

import javax.persistence.Column
import javax.persistence.MappedSuperclass
import java.sql.Time

@MappedSuperclass
abstract class BaseEntity {

    @CreatedDate
    @Column(name = "created_date")
    internal var createdDate: Time? = null

    @UpdateTimestamp
    @Column(name = "updated_date")
    internal var updatedDate: Time? = null
}


