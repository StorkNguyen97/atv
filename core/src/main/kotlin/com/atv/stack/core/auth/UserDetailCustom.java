package com.atv.stack.core.auth;

import org.springframework.security.core.userdetails.UserDetails;

public interface UserDetailCustom extends UserDetails {
    public String getEmail();
}
