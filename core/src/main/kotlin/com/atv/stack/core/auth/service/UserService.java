package com.atv.stack.core.auth.service;

import com.atv.stack.core.auth.domain.Account;
import com.atv.stack.core.auth.domain.FormRegister;
import com.atv.stack.core.auth.exception.UserExistedException;
import com.atv.stack.core.auth.repository.AccountsRepository;
import com.atv.stack.core.view.ResponseFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.Locale;

@Service
public class UserService {
    @Autowired
    private MessageSource messageSource;

    public String getSimpleMessage(String message){
        Locale currentLocale =  LocaleContextHolder.getLocale();
        return messageSource.getMessage(message,null,currentLocale);
    }
    @Autowired
    private AccountsRepository accountsRepository;

    @Autowired
    private PasswordEncoder passwordEncoderBean;

    public ResponseEntity<FormRegister> createNewUser(@Valid FormRegister userView) throws UserExistedException {
        userView.setPassword(passwordEncoderBean.encode(userView.getPassword()));
        try {
            Account account = accountsRepository.findByNameOrEmail(userView.getName(), userView.getEmail());
            if (account != null) {
                return new ResponseFactory().create(null, HttpStatus.BAD_REQUEST, "User đã tồn tại");
            }
            Account newContact = accountsRepository.saveAndFlush(userView.toAccountDto());
            FormRegister output = new FormRegister(newContact);
            return new ResponseFactory().create(output, HttpStatus.OK, getSimpleMessage("success.save_successfully"));
        } catch (Exception e) {
            throw new UserExistedException();
        }
    }
}
