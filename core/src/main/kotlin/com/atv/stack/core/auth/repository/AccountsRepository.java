package com.atv.stack.core.auth.repository;

import com.atv.stack.core.auth.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

public interface AccountsRepository extends JpaRepository<Account, Serializable> {
    Account findByNameOrEmail(String username, String email);
}
