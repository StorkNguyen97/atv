package com.atv.stack.core.view;

import com.sl.kiwi.crawler.core.view.RestResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public class ResponseFactory<T>{
    public ResponseEntity<T> create(HttpStatus status) {
        return  new ResponseEntity<T>(status);
    }

    public ResponseEntity<RestResponse<T>> create(@Nullable T body, HttpStatus status, String message) {
        RestResponse<T> response = new RestResponse<T>(status.value(),message,body, null);
        return  new ResponseEntity(response, status);
    }

    public ResponseEntity<RestResponse<List<T>>> create(@Nullable List<T> body, HttpStatus status, String message) {
        RestResponse<List<T>> response = new RestResponse<List<T>>(status.value(),message,body, null);
        return  new ResponseEntity(response, status);
    }

    public ResponseEntity<RestResponse<T>> create(@Nullable T body, HttpStatus status) {
        RestResponse<T> response = new RestResponse<T>(status.value(),body);
        return  new ResponseEntity(response, status);
    }
    public ResponseEntity<RestResponse<T>> create(HttpStatus status, String message) {
        RestResponse<T> response = new RestResponse<T>(status.value(),message);
        return  new ResponseEntity(response, status);
    }

    public ResponseEntity<RestResponse<T>> create(HttpStatus status, List<String> message) {
        RestResponse<T> response = new RestResponse<T>(status.value(),message);
        return  new ResponseEntity(response, status);
    }

    public ResponseEntity<RestResponse<T>> create(MultiValueMap<String, String> headers, HttpStatus status) {
        RestResponse<T> response = new RestResponse<T>(status.value());
        return  new ResponseEntity(response,headers, status);
    }

}
