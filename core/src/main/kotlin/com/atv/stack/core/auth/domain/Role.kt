package com.atv.stack.core.auth.domain

import javax.persistence.*

@Entity(name = "role")
class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Int = 0;
    var name: String = "";

}