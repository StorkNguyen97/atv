package com.atv.stack.core.auth.domain;


import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class FormRegister {
    @Setter
    @Getter
    private int id;

    @Setter @Getter
    @NotNull(message = "error.username.empty")
    private String name;

    @Setter @Getter
    @Size(min=6, message="error.password.size")
    private String password;

    @Setter @Getter
    @NotNull(message = "error.email.empty")
    private String email;

    public FormRegister(){};

    public FormRegister(Account newContact) {
        BeanUtils.copyProperties(newContact,this);
    }

    public Account toAccountDto() {
        Account account = new Account();
        BeanUtils.copyProperties(this,account);
        return account;
    }
}