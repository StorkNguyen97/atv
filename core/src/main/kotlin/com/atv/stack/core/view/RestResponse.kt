package com.sl.kiwi.crawler.core.view

import com.fasterxml.jackson.annotation.JsonInclude

class RestResponse<T> {

    var status: Int = 0
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var message: String? = null

    @JsonInclude(JsonInclude.Include.NON_NULL)
    var messages: List<String>? = null;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    var data: T? = null


    constructor(status: Int, message: String?, data: T?, messages: List<String>?) {
        this.status = status
        this.message = message
        this.data = data
        this.messages = messages;
    }

    constructor(messages: List<String>) {
        this.messages = messages
    }

    constructor(status: Int, data: T) {
        this.status = status
        this.data = data
    }

    constructor(status: Int, message: String) {
        this.status = status
        this.message = message
    }

    constructor(status: Int, message: List<String>) {
        this.status = status
        this.messages = message
    }

    constructor(status: Int) {
        this.status = status
    }
}
