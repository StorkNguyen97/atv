package com.atv.stack.core.auth.controller;


import com.atv.stack.core.auth.domain.FormRegister;
import com.atv.stack.core.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class UserController {
    @Autowired
    public UserService userService;

    @RequestMapping(value = "public/account", method = RequestMethod.POST)
    public ResponseEntity<FormRegister> createNewUser(@Valid @RequestBody FormRegister userView) throws Throwable {
        return userService.createNewUser(userView);
    }
}
